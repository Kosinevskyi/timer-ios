//
//  AppDelegate.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 13.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var startTimerMenuItem: NSMenuItem!
    @IBOutlet weak var resetTimerMenuItem: NSMenuItem!
    @IBOutlet weak var stopTimerMenuItem: NSMenuItem!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        updateMenusBtns(start: true, reset: false, stop: false)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func updateMenusBtns(start: Bool, reset: Bool, stop: Bool) {
        startTimerMenuItem.isEnabled = start
        resetTimerMenuItem.isEnabled = reset
        stopTimerMenuItem.isEnabled = stop
     
        
    }
}

