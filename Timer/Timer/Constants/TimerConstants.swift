//
//  TimerConstants.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 16.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//
class TimerConstants {
    static let defaultTimerTime: Double = 1500
    static let notificationName: String = "PrefsChanged"
    static let eyeTrainingSegue: String = "eyeTrainingPopOver"
}
