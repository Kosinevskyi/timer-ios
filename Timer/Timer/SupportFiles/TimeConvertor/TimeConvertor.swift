//
//  TimeConvertor.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 16.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation

typealias leftTimeHHMM = (hours: UInt, minutes: UInt)
typealias leftTimeHHMMSS = (hours: UInt, minutes: UInt, seconds: UInt)

class TimeConvertor {
    
    static let shared = TimeConvertor()
    
    private init() {
        
    }
    
    func convertSecondsToHHMM(seconds: Double) -> leftTimeHHMM {
        let hours = Int(seconds) / 3600
        let minutes = Int(seconds) / 60 % 60
        
        return (UInt(hours), UInt(minutes))
    }
    
    func convertSecondsToHHMMSS(seconds: Double) -> leftTimeHHMMSS {
        let hhmm = convertSecondsToHHMM(seconds: seconds)
        let seconds = Int(seconds) % 60

        return (hhmm.hours, hhmm.minutes, UInt(seconds))
    }
    
    func convertDateToHHMMSS(date: Date) -> leftTimeHHMMSS {
        let calendar = Calendar.current
        let hour = UInt(calendar.component(.hour, from: date))
        let minutes = UInt(calendar.component(.minute, from: date))
        let seconds = UInt(calendar.component(.second, from: date))
        
        return (hour, minutes, seconds)
    }
    
    func convertDateToSeconds(date: Date) -> Double {
        let time = convertDateToHHMMSS(date: date)
        
        return Double((time.hours * 3600) + (time.minutes * 60) + time.seconds)
    }
}
