//
//  PreferenceViewController.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 26.03.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Cocoa

class PreferenceViewController: NSViewController {

    var prefs = Preferences()
    
    @IBOutlet weak var timeDatePicker: NSDatePicker!
    @IBOutlet weak var eyeTrainingBtn: NSButton!

    
    @IBAction func okButtonPressed(_ sender: Any) {
        //print("\(TimeConvertor.shared.convertDateToSeconds(date: timeDatePicker.dateValue))")
        //eyeTrainingBtn.state == .on ? print("toggle true") : print("toggle false")
        
        saveNewPrefs()
        view.window?.close()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        view.window?.close()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        showExistingPrefs()
    }
    
    private func saveNewPrefs() {
        prefs.selectedTime = TimeConvertor.shared.convertDateToSeconds(date: timeDatePicker.dateValue)
        prefs.eyeTraining = eyeTrainingBtn.state.rawValue
        
        //MARK: Need notify VC
        NotificationCenter.default.post(name: Notification.Name(rawValue: TimerConstants.notificationName), object: nil)
    }
    
    private func showExistingPrefs() {
        //print("showExistingPrefs")
        
        eyeTrainingBtn.state = .init(prefs.eyeTraining)
        
        let calendar: Calendar = NSCalendar.current
        var components = calendar.dateComponents([.hour, .minute, .second], from: Date())
        let time = TimeConvertor.shared.convertSecondsToHHMMSS(seconds: prefs.selectedTime)

        components.hour = Int(time.hours)
        components.minute = Int(time.minutes)
        components.second = Int(time.seconds)

        timeDatePicker.dateValue = calendar.date(from: components)!
    }
}
