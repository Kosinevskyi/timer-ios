//
//  EyeTrainingViewController.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 30.03.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Cocoa

class EyeTrainingViewController: NSViewController {

    @IBOutlet weak var imageView: NSImageView!
    @IBOutlet weak var headerText: NSTextField!
    @IBOutlet weak var bodyText: NSTextField!
    
    let eyeTraining = EyeTraining()
    var eyeTrainingProtocol: EyeTrainingProtocol?
    
    @IBAction func closeBtn(_ sender: Any) {
        eyeTrainingProtocol?.closeWindow()
        view.window?.close()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eyeTraining.getData { (header, body, image) in
            self.imageView.image = image
            self.headerText.stringValue = header
            self.bodyText.stringValue = body
        }
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        NSApplication.shared.activate(ignoringOtherApps: true)
        self.view.window?.title = "Training session"
    }
    
}
