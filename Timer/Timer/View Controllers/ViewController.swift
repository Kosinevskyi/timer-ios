//
//  ViewController.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 13.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Cocoa
import YPRingProgressView

class ViewController: NSViewController {
    
    //MARK: @IBOutlet
    @IBOutlet weak var hoursTF: NSTextField!
    @IBOutlet weak var minutesTF: NSTextField!
    @IBOutlet weak var secondsTF: NSTextField!

    @IBOutlet weak var startBtn: NSButton!
    @IBOutlet weak var resetBtn: NSButton!
    @IBOutlet weak var stopBtn: NSButton!
    
    @IBOutlet private weak var ringProgressView: YPRingProgressView!
    
    //MARK: private property
    private let soundManager = SoundManager()
    private var prefs = Preferences()
    private let timer = MyTimer()
    private let eyeTrainingViewController = EyeTrainingViewController()
    
    //MARK: override func
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer.delegate = self
        
        soundManager.prepareSound()
        setupPrefs()
        setupRingProgressView()
    }
    
    //MARK: @IBAction func
    @IBAction func startBtnTapped(_ sender: Any) {
        timer.timerState = .start
        timer.updateTimer()
        
        updateBtns(state: timer.timerState)
    }
    
    @IBAction func resetBtnTapped(_ sender: Any) {
        timer.timerState = .reset
        timer.updateTimer()
        
        updateBtns(state: timer.timerState)
    }
    
    @IBAction func stopBtnTapped(_ sender: Any) {
        timer.timerState = .stop
        timer.updateTimer()
        
        updateBtns(state: timer.timerState)
    }

    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        print(segue)
        
        if segue.identifier == "eyeTrainingPopOver" {
            let eyeTrainingVC = segue.destinationController as? EyeTrainingViewController
            
            if eyeTrainingVC != nil {
                eyeTrainingVC?.eyeTrainingProtocol = self
            }
        }
    }
    
    //MARK: private func
    private func setupRingProgressView() {
        ringProgressView.ringStartColor = NSColor(red: 241, green: 179, blue: 72)
        ringProgressView.ringEndColor = NSColor(red: 177 , green: 90, blue: 185)
        ringProgressView.ringBackgroundColor = NSColor(red: 89, green: 89, blue: 90)
        ringProgressView.progress = 1
    }
    
    private func updateTimerUI(for timer: leftTimeHHMMSS) {
        hoursTF.stringValue = String(format: "%02d", timer.hours)
        minutesTF.stringValue = String(format: "%02d", timer.minutes)
        secondsTF.stringValue = String(format: "%02d", timer.seconds)
        
        ringProgressView.progress = CGFloat(((self.timer.duration - self.timer.passedTime) * 1) / self.timer.duration)
    }
    
    private func updateBtns(state: TimerState) {
        switch state {
        case .start:
            startBtn.isEnabled = false
            resetBtn.isEnabled = false
            stopBtn.isEnabled = true
            
        case .reset:
            startBtn.isEnabled = true
            resetBtn.isEnabled = false
            stopBtn.isEnabled = false
        case .stop:
            startBtn.isEnabled = true
            resetBtn.isEnabled = true
            stopBtn.isEnabled = false
        }
        
        guard let appDel = NSApplication.shared.delegate as? AppDelegate else { return }
        appDel.updateMenusBtns(start: startBtn.isEnabled, reset: resetBtn.isEnabled, stop: stopBtn.isEnabled)
    }
    
    private func setupPrefs() {
        updateDisplay(timeRemaining: prefs.selectedTime, eyeTraining: prefs.eyeTraining)
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: TimerConstants.notificationName), object: nil, queue: nil) { (notification) in
            self.checkForResetAfterPrefsChange()
        }
    }
    
    private func checkForResetAfterPrefsChange() {
        if timer.timerState == .stop || timer.timerState == .reset {
            updateDisplay(timeRemaining: prefs.selectedTime, eyeTraining: prefs.eyeTraining)
        } else {
            let alert = NSAlert()
            alert.messageText = "Reset timer with the new settings right now?"
            alert.informativeText = "This will stop your current timer! If no it will switch to previous settings!"
            alert.alertStyle = .warning

            alert.addButton(withTitle: "Reset")
            alert.addButton(withTitle: "Cancel")

            let response = alert.runModal()
            
            if response == NSApplication.ModalResponse.alertFirstButtonReturn {
                timer.duration = prefs.selectedTime
                timer.eyeTraining = prefs.eyeTraining.convertIntToBool()
                self.resetBtnTapped(self)
            } else {
                prefs.eyeTraining = timer.eyeTraining.convertBoolToInt()
                prefs.selectedTime = timer.duration
            }
        }
    }
    
    private func updateDisplay(timeRemaining: TimeInterval, eyeTraining: Int) {
        timer.duration = prefs.selectedTime
        timer.eyeTraining = prefs.eyeTraining.convertIntToBool()
        timer.timerState = .reset
        timer.updateTimer()
        updateBtns(state: timer.timerState)
    }
}
    
//MARK: extension TimerProtocol
extension ViewController: TimerProtocol {
    
    func timerReseted(_ timer: MyTimer, time: leftTimeHHMMSS) {
        updateTimerUI(for: time)
    }
    
    func timerEnded(_ timer: MyTimer) {
        soundManager.playSound()
        updateTimerUI(for: (0,0,0))
        updateBtns(state: .reset)
    }
    
    func timeLeft(_ timer: MyTimer, time: leftTimeHHMMSS) {
        updateTimerUI(for: time)
    }
    
    func showEyeTrainingVC() {
        performSegue(withIdentifier: TimerConstants.eyeTrainingSegue, sender: nil)
    }
}

extension ViewController: EyeTrainingProtocol {
    func closeWindow() {
        startBtnTapped(NSButton())
    }
}
