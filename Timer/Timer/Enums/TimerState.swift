//
//  TimerState.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 16.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

enum TimerState {
    case start
    case reset
    case stop
}
