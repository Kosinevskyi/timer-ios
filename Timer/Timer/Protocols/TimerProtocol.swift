//
//  TimerProtocol.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 16.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation

protocol TimerProtocol {
    func timeLeft(_ timer: MyTimer, time: leftTimeHHMMSS)
    func timerEnded(_ timer: MyTimer)
    func timerReseted(_ timer: MyTimer, time: leftTimeHHMMSS)
    func showEyeTrainingVC()
}
