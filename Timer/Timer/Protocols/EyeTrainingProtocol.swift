//
//  EyeTrainingProtocol.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 27.04.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation

protocol EyeTrainingProtocol {
    func closeWindow()
}
