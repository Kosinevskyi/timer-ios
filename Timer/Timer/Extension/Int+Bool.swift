//
//  Int+Bool.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 28.04.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation

extension Int {
    func convertIntToBool() -> Bool {
        return self > 0 ? true : false
    }
}

extension Bool {
    func convertBoolToInt() -> Int {
        return self ? 1 : 0
    }
}
