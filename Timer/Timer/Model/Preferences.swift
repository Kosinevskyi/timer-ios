//
//  Preferences.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 29.03.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation

struct Preferences {
    var selectedTime: TimeInterval {
      get {
        let savedTime = UserDefaults.standard.double(forKey: "selectedTime")
        if savedTime > 0 {
          return savedTime
        }
        return 1500
      }
      set {
        UserDefaults.standard.set(newValue, forKey: "selectedTime")
      }
    }
    
    var eyeTraining: Int {
      get {
        return UserDefaults.standard.integer(forKey: "eyeTraining")
      }
      set {
        UserDefaults.standard.set(newValue, forKey: "eyeTraining")
      }
    }
}
