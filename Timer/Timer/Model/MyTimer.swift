//
//  Timer.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 15.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation

class MyTimer {
    //MARK: delegate
    var delegate: TimerProtocol?
    
    //MARK: property
    var timerState = TimerState.reset
    var startTime: Date?
    var timer: Timer? = nil
    
    var duration: TimeInterval = TimerConstants.defaultTimerTime
    var passedTime: TimeInterval = 0
    
    //MARK: Seems it need other class. And need subscribe on END timer action for displaying popup
    //Or we can create another Protocol where we will send that we need display eye training. It would be another VC.
    
    var eyeTraining: Bool = false
    
    //MARK: public func
    func updateTimer() {
        switch timerState {
        case .start:
            startTimer()
        case .reset:
            resetTimer()
        case .stop:
            stopTimer()
        }
    }
    
    //MARK: private func
    private func startTimer() {
        if passedTime > 0 {
            startTime = Date(timeIntervalSinceNow: -passedTime)
        } else {
            startTime = Date()
            passedTime = 0
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerTikTok), userInfo: nil, repeats: true)
        timerTikTok()
    }
    
    private func resetTimer() {
        stopTimer()
        
        passedTime = 0
        
        delegate?.timerReseted(self, time: TimeConvertor.shared.convertSecondsToHHMMSS(seconds: duration))
    }
    
    private func stopTimer() {
        timer?.invalidate()
        timer = nil
        startTime = nil
    }
    
    private func secondsToLeft() -> Double {
        return duration - passedTime
    }
    
    //MARK: @objc func
    @objc private func timerTikTok() {
        guard let startTime = startTime else { return }
        
        passedTime = (-startTime.timeIntervalSinceNow).rounded()
        
        let secondsLeft = secondsToLeft()
        
        if secondsLeft <= 0 {
            delegate?.timerEnded(self)
            
            resetTimer()
            timerState = .reset
            if eyeTraining {
                delegate?.showEyeTrainingVC()
            }
            
        } else {
            delegate?.timeLeft(self, time: TimeConvertor.shared.convertSecondsToHHMMSS(seconds: secondsLeft))
        }
    }
}
