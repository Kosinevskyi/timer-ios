//
//  EyeTraining.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 30.03.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import Foundation
import Cocoa

class EyeTraining {
    private var currentStep: Int {
      get {
        return UserDefaults.standard.integer(forKey: "currentStep")
      }
      set {
        UserDefaults.standard.set(newValue, forKey: "currentStep")
      }
    }
    
    private let headerArray = ["Gazing Into the Darkness",
                       "Side to Side Eye Movement",
                       "Up and Down Eye Movement",
                       "Moving Your Eyes Diagonally",
                       "Rolling Your Eyes in a Circle",
                       "Near and Far Focus",
                       "Concentration One",
                       "Concentration Two",
                       "Massage One",
                       "Massage Two"]
    
    private let textArray = [
                     "Place your elbows on the table. Then place the palms of your hands over your eyes with the fingers of one hand crossing over the fingers on the other hand. Close your eyes and gaze into the darkness. Relax and hold this position for 1-3 minutes.",
                     "Stand or sit up right. Look straight ahead. Without moving your head, look to the left. Focus on what you see. Then look to the right. Focus on what you see. Move your eyes side to side 5 times. Repeat this cycle 3 times.",
                     "Sit up straight and look straight ahead. Then look up and focus on what you see. Look down. Don't be afraid to wrinkle your forehead or frown with your eyebrows. It's good for the eyes. Look up and down 5 times. Repeat this cycle 3 times.",
                     "Look straight ahead. Look down and to the left. Then move your eyes diagonally and look up and to the right. Focus on what you see. Repeat this exercise 5 times, then look straight ahead and do the same exercise looking down and to the right and then looking up and to the left. Repeat this cycle 3 times.",
                     "Sit up straight and relax. Look to the left and slowly roll your eyes in a circle clockwise. To do this look up, then slowly move your eyes clockwise and look down and then move your eyes and look to the right. Do it clockwise 5 times and then counterclockwise 5 times. Repeat this cycle 3 times.",
                     "Focus on a nearby object, e.g. a pencil. It should be 20-30 cm away from your eyes. Then look at something distant. Focus on that distant object and try to see it in detail. Then look on the nearby object again. Change this focus 5 times. Repeat this cycle 3 times.",
                     "Sit up straight. Look straight ahead. Gaze at the point between your eyebrows. Focus on what you see and gaze for a few seconds. Look straight ahead. Then gaze at the point between your eyebrows again. Change this focus 5 times. Repeat this cycle 3 times.",
                     "Sit up straight. Look straight ahead. Gaze at the tip of your nose for a few seconds. Then look straight ahead again. Change this focus 5 times. Repeat this cycle 3 times.",
                     "Close your eyes. Then squeeze your eyes shut tightly. Keep them squeezed shut for 2-3 seconds. Relax the muscles around your eyes. Squeeze your eyes shut again. Repeat 10 times.",
                     "Close your eyes. Gently touch your eyelids and massage your eyes with circular movements. Press lightly. Don’t try to press too hard. Make 10 circular movements, first clockwise then counterclockwise."]
    
    private let imageArray = ["Step_00",
                      "Step_01",
                      "Step_02",
                      "Step_03",
                      "Step_04",
                      "Step_05",
                      "Step_06",
                      "Step_07",
                      "Step_08",
                      "Step_09"]
    
    //MARK: public func
    func getData(data: @escaping(_ header: String, _ body: String, _ image: NSImage?) -> ()) {
        data(headerArray[currentStep], textArray[currentStep], NSImage(named: imageArray[currentStep]))
        
        nextStep()
    }
    
    //MARK: private func
    private func nextStep() {
        currentStep += 1
        if currentStep > headerArray.count - 1 {
            currentStep = 0
        }
    }
}
