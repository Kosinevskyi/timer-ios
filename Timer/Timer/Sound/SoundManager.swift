//
//  SoundManager.swift
//  Timer
//
//  Created by Roman Kosinevskyi on 16.02.2020.
//  Copyright © 2020 Roman Kosinevskyi. All rights reserved.
//

import AVFoundation

class SoundManager {
    var soundPlayer: AVAudioPlayer?
    
    func prepareSound() {
        guard let audioFileUrl = Bundle.main.url(forResource: "timerEndSound", withExtension: "mp3") else {
            return
        }
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: audioFileUrl)
            soundPlayer?.prepareToPlay()
        } catch {
            print("Sound player not available: \(error)")
        }
    }
    
    func playSound() {
        soundPlayer?.play()
    }
}
